package by.epam.java6.task6.commands;

import by.epam.java6.task6.dbc.ConnectionPool;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class represents a number of repeating functions during working with
 * database.This class contains connection with db and prepared statement, which
 * is setted again with every new db query.Implementation of interface Command.
 *
 * @author
 * @see Command
 * @see PreparedStatement
 * @see ConnectionPool
 * @since 1.5
 */
public class ConnectionCommand implements Command {

    private Connection connection;
    private PreparedStatement prest;


    @Override
    public int executeStatement(String query, Object... args) throws SQLException {
        connection = ConnectionPool.getInstance().getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(query);
        return 0;
    }

    @Override
    public ResultSet getResultSet(String query, Object... args) throws SQLException {
        connection = ConnectionPool.getInstance().getConnection();
        Statement st = connection.createStatement();
        return st.executeQuery(query);
    }

    /**
     * Returns connection to pool.
     */
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
        }
    }
}
