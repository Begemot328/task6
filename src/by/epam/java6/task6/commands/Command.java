package by.epam.java6.task6.commands;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Interface of required functions for ConnectionCommand.
 * @author Yury
 * @since JDK1.8
 **/

public interface Command {

    public int executeStatement(String query, Object... args) throws SQLException;

    public ResultSet getResultSet(String query, Object... args) throws SQLException;
}
