package by.epam.java6.task6.dbc;

/**
 * Created by Student on 7/20/2015.
 */
public class ConnectionPoolException extends Exception {
    private static final long serialVersionUID = 1L;
    public ConnectionPoolException(String message, Exception e){
        super(message, e);
    }
}
