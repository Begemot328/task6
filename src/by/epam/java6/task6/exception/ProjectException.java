package by.epam.java6.task6.exception;

/** Global project exception
 * @author Yury
 * @since JDK1.8
 **/

public class ProjectException extends Exception {
    private static final long serialVersionUID = 1l;
    private Exception hiddenException;

    public ProjectException(String msg) {
        super(msg);
    }

    public ProjectException(String msg, Exception e) {
        super(msg);
        hiddenException = e;
    }

    public Exception getHiddenException() {
        return hiddenException;
    }
}



