package by.epam.java6.task6.controller;


import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Resource controller for localization.
 * @author Yury
 * @since JDK1.8
 **/
public class Resources {

    private ResourceBundle bundle;
    private static final String RESOURCE_FILE_NAME = "resources.localization.local";

    public Resources(Locale locale) {
        bundle = ResourceBundle.getBundle(RESOURCE_FILE_NAME, locale);
}

    public String getString(String key) {
        return bundle.getString(key);
    }
}
