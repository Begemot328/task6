package by.epam.java6.task6.controller;

/**
 * Request perameter names for servlet. Contains only String constants.
 * @author Yury
 * @since JDK1.8
 **/
public class RequestParameterName {

    public static final String LOCAL = "local";
    public static final String MESSAGE = "message";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String USER_LOGIN = "userlogin";
    public static final String USER_ID = "userid";
    public static final String COMMAND_NAME = "command";
    public static final String ADMIN = "admin";
    public static final String ERROR_MESSAGE = "errormessage";
    public static final String SIMPLE_INFO = "simpleinfo";
    public static final String FILE_NAME = "filename";
    public static final String ID = "id";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String PAGE = "page";
    public static final String READER = "reader";
    public static final String BOOK = "book";
    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String BOOKLIST = "booklist";
    public static final String READERLIST = "readerlist";
    public static final String OPERATIONLIST = "operationlist";
    public static final String OPERATION = "operation";
    public static final String READING_ROOM = "reading_room";
    public static final String ORDER = "order";
    public static final String TIMELIMITLIST = "timelimitlist";
    public static final String TIMELIMIT = "timelimit";

}
