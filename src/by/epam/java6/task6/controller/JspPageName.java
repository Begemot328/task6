package by.epam.java6.task6.controller;

/**
 * Page names for servlet. Contains only String constants.
 * @author Yury
 * @since JDK1.8
 **/
public final class JspPageName {

    private JspPageName() {
    }

    public static final String LIBRARIANMENU_PAGE = "/WEB-INF/jsp/librarianMenu.jsp";
    public static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
    public static final String INDEX_PAGE = "/WEB-INF/jsp/index.jsp";
    public static final String READERINFO_PAGE = "/WEB-INF/jsp/readerInfo.jsp";
    public static final String READERLIST_PAGE = "/WEB-INF/jsp/readerList.jsp";
    public static final String BOOKINFO_READER_PAGE = "/WEB-INF/jsp/bookInfoReader.jsp";
    public static final String BOOKINFO_LIBRARIAN_PAGE = "/WEB-INF/jsp/bookInfoLibrarian.jsp";
    public static final String BOOKLIST_PAGE = "/WEB-INF/jsp/bookList.jsp";
    public static final String READERMENU_PAGE = "/WEB-INF/jsp/readerMenu.jsp";
    public static final String SEARCHBOOK_PAGE = "/WEB-INF/jsp/searchBook.jsp";
    public static final String ADDBOOK_PAGE = "/WEB-INF/jsp/addBook.jsp";
    public static final String OPERATIONINFO_LIBRARIAN_PAGE = "/WEB-INF/jsp/operationInfoLibrarian.jsp";
    public static final String OPERATIONINFO_READER_PAGE = "/WEB-INF/jsp/operationInfoReader.jsp";
    public static final String ORDERINFO_LIBRARIAN_PAGE = "/WEB-INF/jsp/orderInfoLibrarian.jsp";
    public static final String ORDERINFO_READER_PAGE = "/WEB-INF/jsp/orderInfoReader.jsp";
    public static final String TIMELIMITS_PAGE = "/WEB-INF/jsp/timelimits.jsp";

}
