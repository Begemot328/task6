package by.epam.java6.task6.controller;


import by.epam.java6.task6.exception.ProjectException;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.CommandHelper;
import by.epam.java6.task6.logic.ICommand;
import sun.plugin2.message.JavaScriptBaseMessage;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Task6 servlet controller class.
 * @author Yury
 * @since JDK1.8
 */

public class Controller extends HttpServlet {


    private final static long serialVersionUID = 1L;

    public Controller() {
        super();
    }

    public void init() throws ServletException {
    }

    /*
        * recieves requests and handle it
        * @param request - recieved request
        * @param response - response
        * @see import javax.servlet.http.HttpServlet
        * @see import javax.servlet.http.HttpServletRequest
        * @see import javax.servlet.http.HttpServletResponse
         */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = null;
        //request.setCharacterEncoding("UTF-8");

        String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        ICommand command = CommandHelper.getInstance().getCommand(commandName);
        try {
            page = command.execute(request);
        } catch (ProjectException e) {
            page = JspPageName.ERROR_PAGE;
            request.setAttribute(RequestParameterName.ERROR_MESSAGE, e.getMessage());
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    public void destroy() {
        super.destroy();
    }
}
