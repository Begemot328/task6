package by.epam.java6.task6.logic;


import by.epam.java6.task6.logic.impl.*;

import java.util.HashMap;
import java.util.Map;


/**
 * Task6 command helper - chooses ICommand implemented class for command .
 * @author Юрий
 * @since JDK1.8
 * @see CommandName
 * @see ICommand
 */
public final class CommandHelper {
        private static final CommandHelper instance = new CommandHelper();

    private Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();

    private CommandHelper() {
        commands.put(CommandName.LOGIN_COMMAND, new LoginCommand());
        commands.put(CommandName.CHANGE_LOCALE_COMMAND, new ChangeLocale());
        commands.put(CommandName.SEARCH_READER_BY_LASTNAME_COMMAND, new SearchReader());
        commands.put(CommandName.SEARCH_READER_ALL_COMMAND, new SearchReader());
        commands.put(CommandName.SEARCH_READER_BY_NAME_COMMAND, new SearchReader());
        commands.put(CommandName.SEARCH_READER_BY_ID_COMMAND, new SearchReader());
        commands.put(CommandName.SEARCH_BOOK_BY_AUTHOR_COMMAND, new SearchBook());
        commands.put(CommandName.SEARCH_BOOK_BY_TITLE_COMMAND, new SearchBook());
        commands.put(CommandName.SEARCH_BOOK_BY_ID_COMMAND, new SearchBook());
        commands.put(CommandName.SEARCH_BOOK_ALL_COMMAND, new SearchBook());
        commands.put(CommandName.ADD_BOOK_COMMAND, new AddBook());
        commands.put(CommandName.EDIT_BOOK_COMMAND, new EditBook());
        commands.put(CommandName.DELETE_BOOK_COMMAND, new DeleteBook());
        commands.put(CommandName.GIVE_BOOK_COMMAND, new GiveBook());
        commands.put(CommandName.RECIEVE_BOOK_COMMAND, new RecieveBook());
        commands.put(CommandName.MAKE_ORDER_COMMAND, new MakeOrder());
        commands.put(CommandName.SEARCH_OPERATION_BY_ID_COMMAND, new SearchOperation());
        commands.put(CommandName.CONFIRM_ORDER_COMMAND, new ConfirmOrder());
        commands.put(CommandName.SEARCH_OPERATION_BY_ID_COMMAND, new SearchOperation());
        commands.put(CommandName.CANCEL_ORDER_COMMAND, new CancelOrder());
        commands.put(CommandName.GET_TIMELIMIT_COMMAND, new GetTimelimit());
        commands.put(CommandName.SET_TIMELIMIT_COMMAND, new SetTimelimit());
        commands.put(CommandName.GO_TO_COMMAND, new GoTo());
        commands.put(CommandName.GO_TO_MENU_COMMAND, new GoToMenu());
        commands.put(CommandName.GO_TO_MENU_COMMAND, new GoToMenu());
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
    }

    public static CommandHelper getInstance() {
        return instance;
    }


        /*
        * recieves requests and handle it
        * @param commandName - recieved command
        * @return ICommand  - implemented class for command.
        * @see
         */
    public ICommand getCommand(String commandName) {
        ICommand command;
        CommandName name = CommandName.NO_SUCH_COMMAND;
        try {
            name = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException e) {
        }

        if (null != name){
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }
        return command;
    }
}
