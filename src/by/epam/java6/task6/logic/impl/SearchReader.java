package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLReaderDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.entity.Reader;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.CommandName;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.logic.MessageKeyName;

import javax.crypto.spec.OAEPParameterSpec;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/** Reader searching function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class SearchReader implements ICommand {
    private static String NO_COMMAND = "No such command";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Reader reader;
        List<Reader> list = new ArrayList<>();
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();
        List <Operation> operationList = new ArrayList<>();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        MySQLReaderDAO actionReader = new MySQLReaderDAO();
        try {
            if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_READER_BY_LASTNAME_COMMAND.toString())) {
                if(request.getParameter(RequestParameterName.LAST_NAME).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                list = actionReader.getByLastName(request.getParameter(RequestParameterName.LAST_NAME));
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_SUCH_READER_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.READERLIST, list);
                return JspPageName.READERLIST_PAGE;
            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_READER_BY_NAME_COMMAND.toString())) {
                if(request.getParameter(RequestParameterName.LAST_NAME).equals("")
                        || request.getParameter(RequestParameterName.FILE_NAME).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                list = actionReader.getByName(request.getParameter(RequestParameterName.FIRST_NAME),
                        request.getParameter(RequestParameterName.LAST_NAME));
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_SUCH_READER_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.READERLIST, list);
                return JspPageName.READERLIST_PAGE;
            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_READER_ALL_COMMAND.toString())) {
                list = actionReader.getAll();
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_SUCH_READER_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.READERLIST, list);
                return JspPageName.READERLIST_PAGE;
            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_READER_BY_ID_COMMAND.toString())) {
                if(request.getParameter(RequestParameterName.ID).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                reader = actionReader.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));
                if (reader == null) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_SUCH_READER_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }

                operationList = actionOperation.getByReaderId(reader.getId());
                request.getSession(true).setAttribute(RequestParameterName.OPERATIONLIST, operationList);
                request.getSession(true).setAttribute(RequestParameterName.READER, reader);
                return JspPageName.READERINFO_PAGE;
            } else {
                throw new  CommandException(NO_COMMAND);
            }
        } catch (DAOException e) {
            throw new  CommandException( e);
        }
    }
}
