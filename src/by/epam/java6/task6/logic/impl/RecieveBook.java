package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.MessageKeyName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/** Book recieving function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class RecieveBook implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Book book;
        Operation operation;
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        operation = actionOperation.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));
            java.sql.Date date = new java.sql.Date(new Date().getTime());
        if((operation.getReturningDate() == null)) {
            operation.setReturningDate(date);
            actionOperation.update(operation);
            request.getSession().setAttribute(RequestParameterName.MESSAGE, MessageKeyName.BOOK_RETURNED_MESSAGE);
        } else {
            request.getSession().setAttribute(RequestParameterName.MESSAGE, MessageKeyName.BOOK_ALREADY_RETURNED_MESSAGE);
        }
            if ((boolean) request.getSession(true).getAttribute(RequestParameterName.ADMIN)) {
                return JspPageName.OPERATIONINFO_LIBRARIAN_PAGE;
            } else {
                return JspPageName.OPERATIONINFO_READER_PAGE;
            }
    }
}
