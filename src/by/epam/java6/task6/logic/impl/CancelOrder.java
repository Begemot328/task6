package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLLibrarianDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLReaderDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.MessageKeyName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.entity.Reader;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/** Order cancelling function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/

public class CancelOrder implements ICommand {
    @Override


    public String execute(HttpServletRequest request) throws CommandException {

        Book book;
        Operation operation;
        Reader reader;
        Librarian librarian;
        MySQLBookDAO actionBook = new MySQLBookDAO();
        MySQLReaderDAO actionReader = new MySQLReaderDAO();
        MySQLLibrarianDAO actionLibrarian = new MySQLLibrarianDAO();
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        operation = actionOperation.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));
        if (operation.getReturningDate() == null) {
            operation.setReturningDate(new Date(new java.util.Date().getTime()));
            actionOperation.update(operation);
            request.getSession().setAttribute(RequestParameterName.MESSAGE, MessageKeyName.ORDER_CANCELLED_MESSAGE);
            return JspPageName.ORDERINFO_READER_PAGE;
        } else {
            request.getSession().setAttribute(RequestParameterName.MESSAGE, MessageKeyName.ORDER_ALREADY_CANCELLED_MESSAGE);
            return JspPageName.ORDERINFO_READER_PAGE;
        }
    }
}
