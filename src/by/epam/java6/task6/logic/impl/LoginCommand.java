package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLReaderDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLLibrarianDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.MessageKeyName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.controller.Resources;

import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Reader;
import by.epam.java6.task6.entity.User;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.logic.CommandException;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

/** Logging in function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class LoginCommand implements ICommand {


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        Resources bundle;


        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        if ((request.getSession().getAttribute(RequestParameterName.LOCAL) == null)) {
            bundle = new Resources(request.getLocale());
        } else {
            bundle = new Resources(new Locale(request.getSession().getAttribute(RequestParameterName.LOCAL).toString()));
        }

        try {
            MySQLReaderDAO actionReader = new MySQLReaderDAO();
            MySQLLibrarianDAO actionLibrarian = new MySQLLibrarianDAO();
            List<User> list = new ArrayList<>();

            list.addAll(actionLibrarian.getAll());
            list.addAll(actionReader.getAll());

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getLogin().equals(request.getParameter(RequestParameterName.LOGIN))
                        && list.get(i).getPassword().equals(request.getParameter(RequestParameterName.PASSWORD))) {
                    request.getSession(true).setAttribute(RequestParameterName.USER_LOGIN,
                            request.getParameter(RequestParameterName.LOGIN));
                    request.getSession(true).setAttribute(RequestParameterName.USER_ID,
                            list.get(i).getId());
                    request.getSession(true).setAttribute(RequestParameterName.MESSAGE, MessageKeyName.SUCCESSFUL_LOGGING_MESSAGE);

                    if (list.get(i).getClass().equals(Librarian.class)) {
                        request.getSession().setAttribute(RequestParameterName.ADMIN, true);
                        return JspPageName.LIBRARIANMENU_PAGE;
                    } else if (list.get(i).getClass().equals(Reader.class)) {
                        request.getSession().setAttribute(RequestParameterName.ADMIN, false);
                        return JspPageName.READERMENU_PAGE;
                    }
                }
                if (list.get(i).getLogin().equals(request.getParameter(RequestParameterName.LOGIN))) {
                    request.getSession(true).setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_PASSWORD_MESSAGE);
                    return JspPageName.INDEX_PAGE;
                }
            }
            request.getSession(true).setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_LOGIN_MESSAGE);

            return JspPageName.INDEX_PAGE;

        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
