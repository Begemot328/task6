package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

/**
 * Passing to the menu page function
 *
 * @author Yury
 * @see ICommand
 * @since JDK1.8
 **/
public class GoToMenu implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");

        if(request.getSession().getAttribute(RequestParameterName.ADMIN) == null) {
            return JspPageName.INDEX_PAGE;
        }
        if ((boolean) request.getSession().getAttribute(RequestParameterName.ADMIN)) {
            return JspPageName.LIBRARIANMENU_PAGE;
        } else {
            return JspPageName.READERMENU_PAGE;
        }

    }
}
