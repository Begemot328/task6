package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.logic.MessageKeyName;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Book editing function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class EditBook implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Book book;
        book = (Book)request.getSession(true).getAttribute(RequestParameterName.BOOK);
        MySQLBookDAO actionBook = new MySQLBookDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        try {
            if(request.getParameter(RequestParameterName.TITLE).equals("")
                    || request.getParameter(RequestParameterName.AUTHOR).equals("")) {
                throw new IOException();
            }
            book.setAuthor(request.getParameter(RequestParameterName.AUTHOR));
            book.setTitle(request.getParameter(RequestParameterName.TITLE));
        }
            catch (IOException e) {
                request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                return JspPageName.ERROR_PAGE;
            }

            try {
                actionBook.update(book);
        } catch (DAOException e) {
                request.setAttribute(RequestParameterName.MESSAGE, e.getMessage());
                return JspPageName.ADDBOOK_PAGE;
        }
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();
        List<Operation> operationList = new ArrayList<>();
        operationList = actionOperation.getByBookId(book.getId());
        request.getSession(true).setAttribute(RequestParameterName.OPERATIONLIST, operationList);
        request.getSession(true).setAttribute(RequestParameterName.BOOK, book);
        if ((boolean) request.getSession().getAttribute(RequestParameterName.ADMIN)) {
            return JspPageName.BOOKINFO_LIBRARIAN_PAGE;
        } else {
            return JspPageName.BOOKINFO_READER_PAGE;
        }
    }
}
