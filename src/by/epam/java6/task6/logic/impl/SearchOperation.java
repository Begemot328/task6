package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.CommandName;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.logic.MessageKeyName;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/** Operation searching function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class SearchOperation implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Operation operation;
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");

        try {
                   operation = actionOperation.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));
                if (operation == null) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_OPERATION_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.OPERATION, operation);
            if(operation.isOrder()){
                if ((boolean) request.getSession().getAttribute(RequestParameterName.ADMIN)) {
                    return JspPageName.ORDERINFO_LIBRARIAN_PAGE;
                } else {
                    return JspPageName.ORDERINFO_READER_PAGE;
                }
            } else {
                if ((boolean) request.getSession().getAttribute(RequestParameterName.ADMIN)) {
                    return JspPageName.OPERATIONINFO_LIBRARIAN_PAGE;
                } else {
                    return JspPageName.OPERATIONINFO_READER_PAGE;
                }
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
