package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLLibrarianDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLReaderDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.entity.Reader;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.logic.MessageKeyName;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/** Book giving function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class GiveBook implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        Book book;
        Operation operation;
        Reader reader;
        Librarian librarian;
        MySQLBookDAO actionBook = new MySQLBookDAO();
        MySQLReaderDAO actionReader = new MySQLReaderDAO();
        MySQLLibrarianDAO actionLibrarian = new MySQLLibrarianDAO();
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();
        List<Operation> list = new ArrayList<>();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        librarian = actionLibrarian.getById((int) request.getSession().getAttribute(RequestParameterName.USER_ID));
        book = (Book)request.getSession().getAttribute(RequestParameterName.BOOK);
        reader = actionReader.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));

        list = actionOperation.getByBookId(book.getId());
        if (list.size() != 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getReturningDate() == null) {
                    if (list.get(i).isOrder()) {
                        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, MessageKeyName.BOOK_ORDERED_MESSAGE);
                    } else {
                        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, MessageKeyName.BOOK_TAKEN_MESSAGE);
                    }
                    return JspPageName.BOOKINFO_LIBRARIAN_PAGE;
                }
            }
        }

        operation = new Operation(book, reader, librarian, false, Boolean.parseBoolean(request.getParameter(
                    RequestParameterName.READING_ROOM)), new Date(new java.util.Date().getTime()),
                    new Date(new java.util.Date().getTime() + actionOperation.getTimelimit(true,
                            Boolean.parseBoolean(request.getParameter(RequestParameterName.READING_ROOM)))));
            actionOperation.save(operation);
            request.getSession(true).setAttribute(RequestParameterName.OPERATION, operation);
            return JspPageName.OPERATIONINFO_LIBRARIAN_PAGE;

    }
}
