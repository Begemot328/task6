package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

/** Timelimit setting  function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class SetTimelimit implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();
        actionOperation.updateTimelimit(Boolean.parseBoolean(request.getParameter(RequestParameterName.ORDER)),
                Boolean.parseBoolean(request.getParameter(RequestParameterName.READING_ROOM)),
                Integer.parseInt(request.getParameter(RequestParameterName.TIMELIMIT)));
        return new GetTimelimit().execute(request);

    }
}
