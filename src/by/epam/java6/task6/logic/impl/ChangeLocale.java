package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.logic.ICommand;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.CommandException;

import javax.servlet.http.HttpServletRequest;

/** Locale changing function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/

public class ChangeLocale implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        request.getSession(true).setAttribute(RequestParameterName.LOCAL, request.getParameter(RequestParameterName.LOCAL));
        return request.getParameter(RequestParameterName.PAGE);

          }
}
