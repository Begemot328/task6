package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

/** Book deleting function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class DeleteBook implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Book book;
        book = (Book) request.getSession(true).getAttribute(RequestParameterName.BOOK);
        MySQLBookDAO actionBook = new MySQLBookDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        try {
            actionBook.delete(book);
        } catch (DAOException e) {
            request.setAttribute(RequestParameterName.MESSAGE, e.getMessage());
            return JspPageName.ADDBOOK_PAGE;
        }
        book = null;
        if ((boolean) request.getSession(true).getAttribute(RequestParameterName.ADMIN)) {
            return JspPageName.LIBRARIANMENU_PAGE;
        } else {
            return JspPageName.READERMENU_PAGE;
        }
    }
}
