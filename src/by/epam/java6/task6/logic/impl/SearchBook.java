package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.MessageKeyName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.CommandName;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/** Book searching  function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class SearchBook implements ICommand {
    private static String NO_COMMAND = "No such command";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {


        Book book;
        List<Book> list = new ArrayList<>();
        MySQLBookDAO actionBook = new MySQLBookDAO();
        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");

        try {
            if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_BOOK_BY_AUTHOR_COMMAND.toString())) {
                if (request.getParameter(RequestParameterName.AUTHOR).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                list = actionBook.getByAythor(request.getParameter(RequestParameterName.AUTHOR));
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_BOOK_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.BOOKLIST, list);
                return JspPageName.BOOKLIST_PAGE;

            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_BOOK_BY_TITLE_COMMAND.toString())) {
                if (request.getParameter(RequestParameterName.TITLE).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                list = actionBook.getByTitle(request.getParameter(RequestParameterName.TITLE));
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_BOOK_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.BOOKLIST, list);
                return JspPageName.BOOKLIST_PAGE;

            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_BOOK_BY_ID_COMMAND.toString())) {
                if (request.getParameter(RequestParameterName.ID).equals("")) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.WRONG_DATA_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                book = actionBook.getById(Integer.parseInt(request.getParameter(RequestParameterName.ID)));
                if (book == null) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_BOOK_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                MySQLOperationDAO actionOperation = new MySQLOperationDAO();
                List<Operation> operationList = new ArrayList<>();
                operationList = actionOperation.getByBookId(book.getId());
                request.getSession(true).setAttribute(RequestParameterName.OPERATIONLIST, operationList);
                request.getSession(true).setAttribute(RequestParameterName.BOOK, book);
                if ((boolean) request.getSession(true).getAttribute(RequestParameterName.ADMIN)) {
                    return JspPageName.BOOKINFO_LIBRARIAN_PAGE;
                } else {
                    return JspPageName.BOOKINFO_READER_PAGE;
                }
            } else if (request.getParameter(RequestParameterName.COMMAND_NAME).toUpperCase().equals(
                    CommandName.SEARCH_BOOK_ALL_COMMAND.toString())) {
                list = actionBook.getAll();
                if (list.size() == 0) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageKeyName.NO_BOOK_MESSAGE);
                    return JspPageName.ERROR_PAGE;
                }
                request.getSession(true).setAttribute(RequestParameterName.BOOKLIST, list);
                return JspPageName.BOOKLIST_PAGE;
            } else {
                throw new CommandException(NO_COMMAND);
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
