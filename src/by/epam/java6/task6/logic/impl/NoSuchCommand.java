package by.epam.java6.task6.logic.impl;



import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

/** What to do, when you don't know what to do? Use this!)
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class NoSuchCommand implements ICommand {
    private static String NO_COMMAND = "No such command";

    public String execute(HttpServletRequest request) throws CommandException {
        throw new CommandException(NO_COMMAND);
    }
}
