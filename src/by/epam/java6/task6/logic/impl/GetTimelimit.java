package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/** Timelimit getting function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class GetTimelimit implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        List<Integer> list  = new ArrayList<>();
        MySQLOperationDAO actionOperation = new MySQLOperationDAO();

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        list.add(actionOperation.getTimelimitInDays(false, false));
        list.add(actionOperation.getTimelimitInDays(true, true));
        list.add(actionOperation.getTimelimitInDays(true, false));
        list.add(actionOperation.getTimelimitInDays(false, true));
        request.getSession(true).setAttribute(RequestParameterName.TIMELIMITLIST, list);
        return JspPageName.TIMELIMITS_PAGE;
    }
}
