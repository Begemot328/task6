package by.epam.java6.task6.logic.impl;

import by.epam.java6.task6.DAO.CRUD.MySQLBookDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLLibrarianDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLOperationDAO;
import by.epam.java6.task6.DAO.CRUD.MySQLReaderDAO;
import by.epam.java6.task6.controller.JspPageName;
import by.epam.java6.task6.controller.RequestParameterName;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Operation;
import by.epam.java6.task6.entity.Reader;
import by.epam.java6.task6.logic.CommandException;
import by.epam.java6.task6.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/** Passing to the page function
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class GoTo implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        request.getSession(true).setAttribute(RequestParameterName.MESSAGE, "");
        if(request.getParameter(RequestParameterName.PAGE).equals("")){
            return JspPageName.INDEX_PAGE;
        } else
            return request.getParameter(RequestParameterName.PAGE);

    }
}
