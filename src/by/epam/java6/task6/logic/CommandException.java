package by.epam.java6.task6.logic;


import by.epam.java6.task6.exception.ProjectException;

/** Exception during ICommand implementations executing
 * @author Yury
 * @since JDK1.8
 * @see ICommand
 **/
public class CommandException extends ProjectException {

    private static final long serialVersionUID = 1L;

    public CommandException(String msg){
        super(msg);
    }

    public CommandException(String msg, Exception e){
        super(msg, e);
    }

    public CommandException(Exception e){
        super("CommandException: " + e.getClass() + ": " + e.getMessage(), e);
    }

}
