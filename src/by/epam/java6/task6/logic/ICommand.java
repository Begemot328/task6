package by.epam.java6.task6.logic;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Юрий on 09.07.2015.
 */
public interface ICommand {
    public String execute(HttpServletRequest request) throws CommandException;
}
