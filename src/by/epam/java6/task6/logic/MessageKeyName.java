package by.epam.java6.task6.logic;

/**
 * Created by Юрий on 20.07.2015.
 */
public class MessageKeyName {

    public final static String SUCCESSFUL_LOGGING_MESSAGE = "local.successfulllog.message";
    public final static String WRONG_PASSWORD_MESSAGE = "local.wrongpassword.message";
    public final static String NO_LOGIN_MESSAGE = "local.nologin.message";
    public final static String BOOK_ORDERED_MESSAGE = "local.book.ordered.message";
    public final static String BOOK_TAKEN_MESSAGE = "local.book.taken.message";
    public final static String BOOK_RETURNED_MESSAGE = "local.book.returned.message";
    public final static String BOOK_ALREADY_RETURNED_MESSAGE = "local.book.alreturned.message";
    public final static String NO_BOOK_MESSAGE = "local.book.nobook.message";
    public final static String ORDER_CANCELLED_MESSAGE = "local.reader.order.cancelled";
    public final static String ORDER_ALREADY_CANCELLED_MESSAGE = "local.reader.order.alcancelled";
    public final static String WRONG_DATA_MESSAGE = "local.menu.wrongdata";
    public final static String NO_SUCH_READER_MESSAGE = "local.menu.noreader";
    public final static String NO_OPERATION_MESSAGE = "local.operation.nooperation.message";


}
