package by.epam.java6.task6.DAO.CRUD;

import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.DAO.interfaces.LibrarianDAO;
import by.epam.java6.task6.commands.ConnectionCommand;
import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Reader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD implementation for Librarian objects.
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 * @see Librarian
 **/
public class MySQLLibrarianDAO implements LibrarianDAO {
    private static String GET_ALL_LIBRARIANS_QUERY = "SELECT * FROM librarians";
    private static String GET_LIBRARIAN_BY_ID = "SELECT * FROM librarians WHERE ID_NUMBER = LIBRARIANID";


    ConnectionCommand command = new ConnectionCommand();

    @Override
    public boolean delete(Librarian entity) throws DAOException {
        return false;
    }

    @Override
    public Librarian getById(int id) throws DAOException {
        Librarian librarian = null;
        try {
            ResultSet rs = command.getResultSet(GET_LIBRARIAN_BY_ID.replace("LIBRARIANID", Integer.toString(id)));
            if(rs.next()) {
                librarian = new Librarian(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            }
            return librarian;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }


    @Override
    public boolean update(Librarian entity) throws DAOException {
        return false;
    }

    @Override
    public int save(Librarian entity) throws DAOException {
        return 0;
    }

    public List<Librarian> getAll() throws DAOException {
        Librarian librarian;
        try {
            List<Librarian> list = new ArrayList<>();
            ResultSet rs = command.getResultSet(GET_ALL_LIBRARIANS_QUERY);
            while (rs.next()) {
                librarian = new Librarian(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                list.add(librarian);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {

            command.closeConnection();
        }

    }
}
