package by.epam.java6.task6.DAO.CRUD;

import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.DAO.interfaces.ReaderDAO;
import by.epam.java6.task6.commands.ConnectionCommand;
import by.epam.java6.task6.dbc.ConnectionPoolException;
import by.epam.java6.task6.entity.Reader;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD implementation for Reader objects.
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 * @see Reader
 **/
public class MySQLReaderDAO implements ReaderDAO {
    private static String GET_ALL_READERS_QUERY = "SELECT * FROM readers";
    private static String GET_READER_BY_ID = "SELECT * FROM readers WHERE ID_NUMBER = READERID";
    private static String GET_READER_BY_LASTNAME = "SELECT * FROM readers WHERE LAST_NAME = 'LASTNAME'";
    private static String GET_READER_BY_NAME = "SELECT * FROM readers WHERE LAST_NAME = 'LASTNAME' " +
            "AND FIRST_NAME = 'FIRSTNAME'";
    private static String GET_READER_BY_LOGIN = "SELECT * FROM readers WHERE LOGIN = 'READERLOGIN'";


    private ConnectionCommand command = new ConnectionCommand();

    @Override
    public boolean delete(Reader entity) throws DAOException {
        return false;
    }

    @Override
    public Reader getById(int id) throws DAOException {
        Reader reader = null;
        try {
            ResultSet rs = command.getResultSet(GET_READER_BY_ID.replace("READERID", Integer.toString(id)));
            if(rs.next()) {
                reader = new Reader(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getString(7));
            }
            return reader;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }


    public Reader getByLogin(String login) throws DAOException {
        Reader reader = null;
        try {
            ResultSet rs = command.getResultSet(GET_READER_BY_LOGIN.replace("READERLOGIN", login));
            if(rs.next()) {
                reader = new Reader(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getString(7));
            }
            return reader;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public List <Reader> getByName(String firstName, String lastName) throws DAOException {
        Reader reader = null;
        List <Reader> list = new ArrayList<>();
        try {
            ResultSet rs = command.getResultSet(
                    GET_READER_BY_NAME.replace("FIRSTNAME", firstName).replace("LASTNAME", lastName));
            while(rs.next()) {
                reader = new Reader(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getString(7));
            list.add(reader);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }

    }

    public List <Reader> getByLastName(String lastName) throws DAOException {
        Reader reader = null;
        List <Reader> list = new ArrayList<>();
        try {
            ResultSet rs = command.getResultSet(
                    GET_READER_BY_LASTNAME.replace("LASTNAME", lastName));
            while(rs.next()) {
                reader = new Reader(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getString(7));
                list.add(reader);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }

    }

    @Override
    public boolean update(Reader entity) throws DAOException {
        return false;
    }

    @Override
    public int save(Reader entity) throws DAOException {
        return 0;
    }

    public List<Reader> getAll() throws DAOException {
        Reader reader;
        try {
            List<Reader> list = new ArrayList<>();
            ResultSet rs = command.getResultSet(GET_ALL_READERS_QUERY);
            while (rs.next()) {
                reader = new Reader(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getDate(6), rs.getString(7));
                list.add(reader);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }
}
