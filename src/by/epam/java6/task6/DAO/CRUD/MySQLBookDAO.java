package by.epam.java6.task6.DAO.CRUD;

import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.DAO.interfaces.BookDAO;
import by.epam.java6.task6.commands.ConnectionCommand;
import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Reader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD implementation for Book objects.
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 * @see Book
 **/
public class MySQLBookDAO implements BookDAO {
    private static String GET_BOOK_BY_ID = "SELECT * FROM catalogue WHERE ID_NUMBER = BOOKID";
    private static String GET_BOOK_BY_TITLE = "SELECT * FROM catalogue WHERE TITLE  = 'BOOKTITLE'";
    private static String GET_BOOK_BY_AUTHOR = "SELECT * FROM catalogue WHERE AUTHOR  = 'BOOKAUTHOR'";
    private static String GET_ALL_BOOKS_QUERY = "SELECT * FROM catalogue";
    private static String ADD_BOOK_QUERY = "INSERT INTO catalogue (`ID_NUMBER`, `TITLE`, `AUTHOR`)" +
            " VALUES (BOOKID, 'BOOKTITLE', 'BOOKAUTHOR')";
    private static String GET_MAX_ID = "SELECT MAX(ID_NUMBER) FROM catalogue";
    private static String DELETE_BOOK_QUERY = "DELETE FROM catalogue WHERE ID_NUMBER = BOOKID";
    private static String EDIT_BOOK_QUERY = "UPDATE catalogue SET TITLE = 'BOOKTITLE',  AUTHOR = 'BOOKAUTHOR'  " +
            "WHERE ID_NUMBER = BOOKID";



    private ConnectionCommand command = new ConnectionCommand();

    @Override
    public boolean delete(Book book) throws DAOException {
        try {
            command.executeStatement(DELETE_BOOK_QUERY.replace("BOOKID",
                    Integer.toString(book.getId())));
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    @Override
    public Book getById(int id) throws DAOException {
        Book book = null;
        try {
            ResultSet rs = command.getResultSet(GET_BOOK_BY_ID.replace("BOOKID", Integer.toString(id)));
            if(rs.next()) {
                book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3));
            }
            return book;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }

    }

    public List<Book> getByTitle(String title) throws DAOException {
        Book book = null;
        try {
            List<Book> list = new ArrayList<>();
            ResultSet rs = command.getResultSet(GET_BOOK_BY_TITLE.replace("BOOKTITLE",title));
            while (rs.next()) {
                book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(book);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public List<Book> getByAythor (String author) throws DAOException {
        Book book = null;
        try {
            List<Book> list = new ArrayList<>();
            ResultSet rs = command.getResultSet(GET_BOOK_BY_AUTHOR.replace("BOOKAUTHOR", author));
            while(rs.next()) {
                book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3));
            list.add(book);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    @Override
    public boolean update(Book book) throws DAOException {
        try {
            command.executeStatement(EDIT_BOOK_QUERY.replace("BOOKAUTHOR", book.getAuthor()).replace("BOOKID",
                    Integer.toString(book.getId())).replace("BOOKTITLE", book.getTitle()));
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    @Override
    public int save(Book book) throws DAOException {
        try {
            ResultSet rs = command.getResultSet(GET_MAX_ID);
            if (rs.next()) {
            book.setId(rs.getInt(1) + 1);}
           command.executeStatement(ADD_BOOK_QUERY.replace("BOOKAUTHOR", book.getAuthor()).replace("BOOKID",
                   Integer.toString(book.getId())).replace("BOOKTITLE", book.getTitle()));
            return book.getId();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public List<Book> getAll() throws DAOException {
        Book book;
        try {
            List<Book> list = new ArrayList<>();
            ResultSet rs = command.getResultSet(GET_ALL_BOOKS_QUERY);
            while (rs.next()) {
                book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(book);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }
}
