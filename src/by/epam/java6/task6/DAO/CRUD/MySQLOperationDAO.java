package by.epam.java6.task6.DAO.CRUD;

import by.epam.java6.task6.DAO.exceptions.DAOException;
import by.epam.java6.task6.DAO.interfaces.OperationDAO;
import by.epam.java6.task6.commands.ConnectionCommand;
import by.epam.java6.task6.entity.Operation;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD implementation for Operation objects.
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 **/
public class MySQLOperationDAO implements OperationDAO {
    private static String GET_ALL_OPERATIONS_QUERY = "SELECT * FROM givenbooks";
    private static String GET_OPERATION_BY_ID = "SELECT * FROM givenbooks WHERE ID_NUMBER = OPERATIONID";
    private static String GET_OPERATION_BY_READER = "SELECT * FROM givenbooks WHERE READER_ID = READERID";
    private static String GET_OPERATION_BY_BOOK = "SELECT * FROM givenbooks WHERE BOOK_ID = BOOKID";
    private static String EDIT_OPERATION_QUERY = "UPDATE givenbooks SET RETURNING_DATE = RETURNINGDATE, LIBRARIAN_ID = " +
            "LIBRARIANID WHERE ID_NUMBER = OPERATIONID";
    private static String GET_MAX_ID = "SELECT MAX(ID_NUMBER) FROM givenbooks";
    private static String ADD_OPERATION_QUERY = "INSERT INTO `Task6`.`givenbooks` (`id_number`, `book_id`, `reader_id`, " +
            " `librarian_id`, `taking_date`, `returning_date`, `deadline_date`, `notes`, `order`, " +
            "`reading_room`) VALUES (OPERATIONID, BOOKID, READERID, LIBRARIANID, 'TAKINGDATE', RETURNINGDATE, " +
            "'DEADLINEDATE', NOTES, ISORDER, ISREADINGROOM)";
    private static String GET_TIMELIMIT = "SELECT * FROM timelimits WHERE  READING_ROOM_LIMIT = ISREADINGROOM AND" +
            " ORDER_LIMIT = ISORDER";
    private static String EDIT_TIMELIMIT = "UPDATE timelimits SET TIME_LIMITS = LIMITTIME WHERE  " +
            "READING_ROOM_LIMIT = ISREADINGROOM AND ORDER_LIMIT = ISORDER";

    private ConnectionCommand command = new ConnectionCommand();


    @Override
    public boolean delete(Operation entity) throws DAOException {
        return false;
    }

    @Override
    public Operation getById(int id) throws DAOException {
        Operation operation = null;
        MySQLReaderDAO sqlReaderDAO = new MySQLReaderDAO();
        MySQLBookDAO sqlBookDAO = new MySQLBookDAO();
        MySQLLibrarianDAO sqlLibrarianDAO = new MySQLLibrarianDAO();
        try {
            ResultSet rs = command.getResultSet(GET_OPERATION_BY_ID.replace("OPERATIONID", Integer.toString(id)));
            if (rs.next()) {
                operation = new Operation(rs.getInt(1), sqlBookDAO.getById(rs.getInt(2)), sqlReaderDAO.getById(rs.getInt(3)),
                        sqlLibrarianDAO.getById(rs.getInt(4)), rs.getBoolean(9),
                        rs.getBoolean(10), rs.getDate(5), ((rs.getObject(6) == null) ? null : rs.getDate(6)), rs.getDate(7));
            }
            return operation;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public int getTimelimit(boolean isOrder, boolean isReadingRoom) throws DAOException {

        try {
            ResultSet rs = command.getResultSet(GET_TIMELIMIT.replace("ISREADINGROOM", Boolean.toString(isReadingRoom)).replace(
                    "ISORDER", Boolean.toString(isOrder)));
            if (rs.next()) {
                return rs.getInt(2) * 24 * 3600 * 1000;
            }
            return 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public int getTimelimitInDays(boolean isOrder, boolean isReadingRoom) throws DAOException {

        try {
            ResultSet rs = command.getResultSet(GET_TIMELIMIT.replace("ISREADINGROOM", isReadingRoom + "").replace(
                    "ISORDER", isOrder + ""));
            if (rs.next()) {
                return rs.getInt(2);
            }
            return 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public boolean updateTimelimit(boolean isOrder, boolean isReadingRoom, int timelimit) throws DAOException {
        try {

            if (!(timelimit == 0)) {
                String statement = EDIT_TIMELIMIT.replace("ISREADINGROOM", Boolean.toString(isReadingRoom)).replace(
                        "ISORDER", Boolean.toString(isOrder)).replace("LIMITTIME", Integer.toString(timelimit));
                command.executeStatement(statement);
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public List<Operation> getByReaderId(int id) throws DAOException {
        Operation operation = null;
        List<Operation> list = new ArrayList<>();
        MySQLReaderDAO sqlReaderDAO = new MySQLReaderDAO();
        MySQLBookDAO sqlBookDAO = new MySQLBookDAO();
        MySQLLibrarianDAO sqlLibrarianDAO = new MySQLLibrarianDAO();
        try {
            ResultSet rs = command.getResultSet(GET_OPERATION_BY_READER.replace("READERID", Integer.toString(id)));
            while (rs.next()) {
                operation = new Operation(rs.getInt(1), sqlBookDAO.getById(rs.getInt(2)), sqlReaderDAO.getById(rs.getInt(3)),
                        sqlLibrarianDAO.getById(rs.getInt(4)), rs.getBoolean(9),
                        rs.getBoolean(10), rs.getDate(5), ((rs.getObject(6) == null) ? null : rs.getDate(6)), rs.getDate(7));
                list.add(operation);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    public List<Operation> getByBookId(int id) throws DAOException {
        Operation operation = null;
        List<Operation> list = new ArrayList<>();
        MySQLReaderDAO sqlReaderDAO = new MySQLReaderDAO();
        MySQLBookDAO sqlBookDAO = new MySQLBookDAO();
        MySQLLibrarianDAO sqlLibrarianDAO = new MySQLLibrarianDAO();
        try {
            ResultSet rs = command.getResultSet(GET_OPERATION_BY_BOOK.replace("BOOKID", Integer.toString(id)));
            while (rs.next()) {
                operation = new Operation(rs.getInt(1), sqlBookDAO.getById(rs.getInt(2)), sqlReaderDAO.getById(rs.getInt(3)),
                        sqlLibrarianDAO.getById(rs.getInt(4)), rs.getBoolean(9),
                        rs.getBoolean(10), rs.getDate(5), ((rs.getObject(6) == null) ? null : rs.getDate(6)), rs.getDate(7));
                list.add(operation);
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    @Override
    public boolean update(Operation operation) throws DAOException {
        try {
            String statement = EDIT_OPERATION_QUERY.replace("OPERATIONID", Integer.toString(operation.getId()));
            if (!(operation.getReturningDate() == null)) {
                statement = statement.replace("RETURNINGDATE", "'" + operation.getReturningDate() + "'");
            } else {
                statement = statement.replace("RETURNINGDATE", "NULL");
            }
            if (!(operation.getLibrarian() == null)) {
                statement = statement.replace("LIBRARIANID", Integer.toString(operation.getLibrarian().getId()));
            } else {
                statement = statement.replace("LIBRARIANID", "NULL");
            }
            command.executeStatement(statement);
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }

    @Override
    public int save(Operation operation) throws DAOException {
        try {
            String statement;
            ResultSet rs = command.getResultSet(GET_MAX_ID);
            if (rs.next()) {
                operation.setId(rs.getInt(1) + 1);
            }

            statement = ADD_OPERATION_QUERY.replace("OPERATIONID", Integer.toString(operation.getId())).replace(
                    "READERID", Integer.toString(operation.getReader().getId())).replace(
                    "BOOKID", Integer.toString(operation.getBook().getId())).replace(
                    "TAKINGDATE", String.valueOf(operation.getTakingDate())).replace("ISORDER",
                    Boolean.toString(operation.isOrder())).replace(
                    "ISREADINGROOM", Boolean.toString(operation.isReadingRoom())).replace(
                    "DEADLINEDATE", String.valueOf(operation.getDeadlineDate()));

            if (!(operation.getReturningDate() == null)) {
                statement = statement.replace("RETURNINGDATE", "'" + operation.getReturningDate() + "'");
            } else {
                statement = statement.replace("RETURNINGDATE", "NULL");
            }
            if (!(operation.getLibrarian() == null)) {
                statement = statement.replace("LIBRARIANID", Integer.toString(operation.getLibrarian().getId()));
            } else {
                statement = statement.replace("LIBRARIANID", "NULL");
            }
            command.executeStatement(statement);
            return operation.getId();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            command.closeConnection();
        }
    }
}
