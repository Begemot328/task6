package by.epam.java6.task6.DAO.interfaces;

import by.epam.java6.task6.DAO.exceptions.DAOException;

/** CRUD interface
 * @author Yury
 * @since JDK1.8
 **/

public interface CRUD<T> {
    public boolean delete(T entity)
            throws DAOException;

    public T getById(int id)
            throws DAOException;

    public boolean update(T entity)
            throws DAOException;

    public int save(T entity)
            throws DAOException;
}
