package by.epam.java6.task6.DAO.interfaces;

import by.epam.java6.task6.entity.Operation;

/**
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 **/
public interface OperationDAO extends CRUD<Operation> {
}
