package by.epam.java6.task6.DAO.interfaces;

import by.epam.java6.task6.entity.Reader;

/**
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 **/

public interface ReaderDAO extends CRUD<Reader> {
}
