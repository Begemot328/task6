package by.epam.java6.task6.DAO.exceptions;

import by.epam.java6.task6.logic.CommandException;

/**
 * DAO exception
 * @author Yury
 * @since JDK1.8
 * @see by.epam.java6.task6.DAO.interfaces.CRUD
 **/
public class DAOException extends CommandException {
    private static final long serialVersionUID = 1L;

    public DAOException(String msg){
        super(msg);
    }

    public DAOException(String msg, Exception e){
        super(msg, e);
    }

    public DAOException(Exception e){
        super("DAOException: " + e.getClass() + ": " + e.getMessage(), e);
    }
}
