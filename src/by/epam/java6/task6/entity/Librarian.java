package by.epam.java6.task6.entity;

import java.util.Objects;

/** Librarian entity
 * @author Yury
 * @since JDK1.8
 **/
public class Librarian implements User{
    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private int id;


    public Librarian(int id, String firstName, String lastName, String login, String password) {
        this.firstName = firstName;
        this.id = id;
        this.password = password;
        this.login = login;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o.getClass().equals(Librarian.class))) {
            return false;
        }
        Librarian librarian = (Librarian) o;
        return Objects.equals(id, librarian.id) &&
                Objects.equals(firstName, librarian.firstName) &&
                Objects.equals(lastName, librarian.lastName) &&
                Objects.equals(login, librarian.login) &&
                Objects.equals(password, librarian.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, login, password, id);
    }
}
