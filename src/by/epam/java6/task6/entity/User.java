package by.epam.java6.task6.entity;

/**
 * Created by Юрий on 26.07.2015.
 */
public interface User {
    public int getId();

    public void setId(int id);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getPassword();

    public void setPassword(String password);

    public String getLogin();

    public void setLogin(String login);
}
