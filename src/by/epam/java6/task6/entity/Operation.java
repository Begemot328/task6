package by.epam.java6.task6.entity;

import by.epam.java6.task6.entity.Book;
import by.epam.java6.task6.entity.Librarian;
import by.epam.java6.task6.entity.Reader;

import java.sql.Date;
import java.util.Objects;

/** Operation entity
 * @author Yury
 * @since JDK1.8
 **/
public class Operation {
    private int id;
    private Book book;
    private Reader reader;
    private Librarian librarian;
    private boolean order;
    private boolean readingRoom;
    private Date takingDate;
    private Date returningDate;
    private Date deadlineDate;


    public Operation(int id, Book book, Reader reader, Librarian librarian, boolean order,
                     boolean readingRoom, Date takingDate, Date returningDate, Date deadlineDate) {
        this.id = id;
        this.book = book;
        this.reader = reader;
        this.librarian = librarian;
        this.order = order;
        this.readingRoom = readingRoom;
        this.takingDate = takingDate;
        this.returningDate = returningDate;
        this.deadlineDate = deadlineDate;
    }

    public Operation(Book book, Reader reader, Librarian librarian, boolean order,
                     boolean readingRoom, Date takingDate, Date returningDate, Date deadlineDate) {
        this.book = book;
        this.reader = reader;
        this.librarian = librarian;
        this.order = order;
        this.readingRoom = readingRoom;
        this.takingDate = takingDate;
        this.returningDate = returningDate;
        this.deadlineDate = deadlineDate;
    }

    public Operation(Book book, Reader reader, Librarian librarian, boolean order,
                     boolean readingRoom, Date takingDate, Date deadlineDate) {
        this.book = book;
        this.reader = reader;
        this.librarian = librarian;
        this.order = order;
        this.readingRoom = readingRoom;
        this.takingDate = takingDate;
        this.returningDate = returningDate;
        this.deadlineDate = deadlineDate;
    }

    public Operation(Book book, Reader reader, boolean order,
                     boolean readingRoom, Date takingDate, Date deadlineDate) {
        this.book = book;
        this.reader = reader;
        this.order = order;
        this.readingRoom = readingRoom;
        this.takingDate = takingDate;
        this.deadlineDate = deadlineDate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public void setLibrarian(Librarian librarian) {
        this.librarian = librarian;
    }

    public boolean isOrder() {
        return order;
    }

    public void setOrder(boolean order) {
        this.order = order;
    }

    public boolean isReadingRoom() {
        return readingRoom;
    }

    public void setReadingRoom(boolean readingRoom) {
        this.readingRoom = readingRoom;
    }

    public Date getTakingDate() {
        return takingDate;
    }

    public Date getReturningDate() {
        return returningDate;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setTakingDate(Date takingDate) {
        this.takingDate = takingDate;
    }

    public void setReturningDate(Date returningDate) {
        this.returningDate = returningDate;
    }

    public void setDeadlineDate(Date deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o.getClass().equals(Operation.class))) return false;
        Operation operation = (Operation) o;
        return Objects.equals(id, operation.id) &&
                Objects.equals(order, operation.order) &&
                Objects.equals(readingRoom, operation.readingRoom) &&
                Objects.equals(book, operation.book) &&
                Objects.equals(reader, operation.reader) &&
                Objects.equals(librarian, operation.librarian) &&
                Objects.equals(takingDate, operation.takingDate) &&
                Objects.equals(returningDate, operation.returningDate) &&
                Objects.equals(deadlineDate, operation.deadlineDate);
    }

    @Override
    public String toString() {
        return getClass().getName() + "@ {" +
                "id=" + id +
                ", book=" + book +
                ", reader=" + reader +
                ", librarian=" + librarian +
                ", order=" + order +
                ", readingRoom=" + readingRoom +
                ", takingDate=" + takingDate +
                ", returningDate=" + returningDate +
                ", deadlineDate=" + deadlineDate +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, book, reader, librarian, order, readingRoom, takingDate, returningDate, deadlineDate);
    }
}
