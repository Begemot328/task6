package by.epam.java6.task6.entity;

import java.util.Date;
import java.util.Objects;

/** Reader entity
 * @author Yury
 * @since JDK1.8
 **/
public class Reader implements User{
    private String firstName;
    private String lastName;
    private String adress;
    private String login;
    private String password;
    private int id;
    private Date registrationDate;

    public Reader(int id, String firstName, String lastName, String login, String password, Date registrationDate, String adress) {
        this.firstName = firstName;
        this.registrationDate = registrationDate;
        this.id = id;
        this.password = password;
        this.login = login;
        this.adress = adress;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }


    @Override
    public String toString() {
        return getClass().getName() + "@{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", adress='" + adress + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", registrationDate=" + registrationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o.getClass().equals(Reader.class))) return false;
        Reader reader = (Reader) o;
        return Objects.equals(id, reader.id) &&
                Objects.equals(firstName, reader.firstName) &&
                Objects.equals(lastName, reader.lastName) &&
                Objects.equals(adress, reader.adress) &&
                Objects.equals(login, reader.login) &&
                Objects.equals(password, reader.password) &&
                Objects.equals(registrationDate, reader.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, adress, login, password, id, registrationDate);
    }
}
