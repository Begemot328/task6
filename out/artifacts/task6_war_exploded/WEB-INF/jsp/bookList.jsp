<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
  <title>Book list page</title>
  <fmt:setLocale value="${sessionScope.local}"/>
  <fmt:setBundle basename="resources.localization.local" var="loc"/>
  <fmt:message bundle="${loc}" key="local.reader.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.book.searchbyid" var="searchbyidbutton"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
  <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
  <fmt:message bundle="${loc}" key="local.search.reader" var="search_reader"/>
  <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
  <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
  <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
  <fmt:message bundle="${loc}" key="local.menu.booklist" var="book_list"/>
  <fmt:message bundle="${loc}" key="local.book.title" var="title"/>
  <fmt:message bundle="${loc}" key="local.book.author" var="author"/>
  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
<body>
<div class="container">
  <header><h1>Библиотека "Хата-читальня"</h1> </header>
  <div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
      <a class="navbar-form brand pull-left" href="#">Menu</a>
      <ul class="nav">
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
          <input type="submit" class="btn"  value="${login_page}" /><br />
          </form>
        </li>
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_menu_command" />
          <input type="submit" class="btn"  value="${home}" /><br />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/bookList.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/bookList.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
          </form>
        </li>
        <li class="divider-vertical" > </li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="span11  offset1">
      <legend>${book_list} </legend>
      <table class="table">
        <caption>${book_list}</caption>
        <thead>
        <tr class="success">
          <th>${title}</th>
          <th>${author}</th>
          <th>${searchbyidbutton}</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="book" items="${booklist}">
        <tr class="info">
          <td>${book.title}</td>
          <td>${book.author}</td>
          <td>
            <form action="Task6" method="post">
            <input type="hidden" name="command" value="search_book_by_id_command" />
            <input type="hidden" name="id" value=${book.id}>
            <input type="submit" class="btn" value="${searchbyidbutton}" />
          </form>
          </td>
        </tr>
</c:forEach>
        </tbody>
      </table>
        <h3><c:out value="${messageloc}"/> </h3>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>