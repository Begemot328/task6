
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
    <title>Reader menu</title>
  <fmt:setLocale value="${sessionScope.local}"/>
  <fmt:setBundle basename="resources.localization.local" var="loc"/>
  <fmt:message bundle="${loc}" key="local.menu.searchbook" var="search_book_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
  <fmt:message bundle="${loc}" key="local.menu.searchuser" var="search_user_button"/>
  <fmt:message bundle="${loc}" key="local.menu.searchbook" var="search_book_button"/>
  <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
  <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
  <fmt:message bundle="${loc}" key="local.reader.menu" var="reader_menu"/>
  <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
  <fmt:message bundle="${loc}" key="local.reader.operations" var="view_operations"/>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
  <header><h1>Библиотека "Хата-читальня"</h1> </header>
  <div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
      <a class="brand" href="#">Menu</a>
      <ul class="nav">
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
          <input type="submit" class="btn"  value="${login_page}" /><br />
          </form>
        </li>
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/readerMenu.jsp"/>
          <input type="submit" class="btn"  value="${home}" /><br />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/readerMenu.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/readerMenu.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
          </form>
        </li>
        <li class="divider-vertical" > </li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="span12">
      <legend>${reader_menu} </legend>
<form action="Task6" method="post"/>
      <div class="control-group offset3">
        <div class="controls">
<input type="hidden" name="command" value="search_reader_by_id_command" />
<input type="hidden" name="id" value="${userid}"/>
<input type="submit" class="btn" value="${view_operations}" /><br />
        </div>
      </div>
</form>
<form action="Task6" method="post"/>
      <div class="control-group offset3">
        <div class="controls">
<input type="hidden" name="command" value="go_to_command" />
<input type="hidden" name="page" value="/WEB-INF/jsp/searchBook.jsp"/>
<input type="submit" class="btn" value="${search_book_button}" /><br />
        </div>
      </div>
</form>
      <h3><c:out value="${messageloc}" /> </h3>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
