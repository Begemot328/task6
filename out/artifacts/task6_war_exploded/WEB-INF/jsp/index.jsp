<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
  <head>
    <title>Start page</title>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="resources.localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
    <fmt:message bundle="${loc}" key="local.gobutton.name" var="gobutton"/>
    <fmt:message bundle="${loc}" key="local.login.name" var="login"/>
    <fmt:message bundle="${loc}" key="local.password.name" var="password"/>
    <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
    <fmt:message bundle="${loc}" key="local.authorization" var="authorization"/>
    <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
  <div class="container">
    <header><h1>Библиотека "Хата-читальня"</h1> </header>
    <div class="navbar navbar-static-top navbar-inverse">
      <div class="navbar-inner">
        <a class="navbar-form brand pull-left" href="#">Menu</a>
        <ul class="nav">
          <li class="divider-vertical">
          </li>
          <li class="navbar-form pull-left">
            <form action="Task6" method="post"/>
            <input type="hidden" name="command" value="go_to_menu_command" />
            <input type="submit" class="btn"  value="${home}" /><br />
            </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li class="navbar-form pull-right">
            <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
            </form>
          </li>
          <li class="divider-vertical" > </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="well span4 offset4">
        <legend>${authorization} </legend>
  <form  action="Task6" method="post">
    <div class="control-group">
      <label class="control-label" >${login} </label>
      <div class="controls">
    <input type="login" name="login" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder=${login} value=""/>
    </div>
    </div>
    <div class="control-group">
      <label class="control-label" >${password} </label>
      <div class="controls">
    <input type="password" name="password" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder=${password} value=""/>
    </div>
    </div>
    <div class="control-group">
      <div class="controls">
        <input type="submit" class="btn btn-primary"  value="${gobutton} " />
        <input type="hidden" name="command" value="login_command" />
      </div>
    </div>
  </form>
  <h3><c:out value="${messageloc}" /> </h3>
  </div>
  </div>
  </div>
  <script src="js/bootstrap.min.js"></script>
  </body>
</html>
