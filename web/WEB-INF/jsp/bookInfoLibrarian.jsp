<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
  <title>Book info page for librarian</title>
  <fmt:setLocale value="${sessionScope.local}"/>
  <fmt:setBundle basename="resources.localization.local" var="loc"/>
  <fmt:message bundle="${loc}" key="local.reader.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
  <fmt:message bundle="${loc}" key="local.menu.editbook" var="edit_book_button"/>
  <fmt:message bundle="${loc}" key="local.menu.deletebook" var="delete_book_button"/>
  <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
  <fmt:message bundle="${loc}" key="local.search.reader" var="search_reader"/>
  <fmt:message bundle="${loc}" key="local.search.operation" var="search_operation"/>
  <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
  <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
  <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
  <fmt:message bundle="${loc}" key="local.menu.booklist" var="book_list"/>
  <fmt:message bundle="${loc}" key="local.menu.makeorder" var="makeorder"/>
  <fmt:message bundle="${loc}" key="local.menu.givebook" var="give_book"/>
  <fmt:message bundle="${loc}" key="local.menu.order" var="order"/>
  <fmt:message bundle="${loc}" key="local.menu.readingroom" var="readingroom"/>
  <fmt:message bundle="${loc}" key="local.book.title" var="title"/>
  <fmt:message bundle="${loc}" key="local.book.author" var="author"/>
  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
  <header><h1>Библиотека "Хата-читальня"</h1> </header>
  <div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
      <a class="navbar-form brand pull-left" href="#">Menu</a>
      <ul class="nav">
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
          <input type="submit" class="btn"  value="${login_page}" /><br />
          </form>
        </li>
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_menu_command" />
          <input type="submit" class="btn"  value="${home}" /><br />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/bookInfoLibrarian.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/bookInfoLibrarian.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
          </form>
        </li>
        <li class="divider-vertical" > </li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="span11  offset1">
      <legend>${book.title} ${book.author} </legend>
        <table class="table">
          <caption>${operation_list}</caption>
          <thead>
          <tr class="success">
            <th>${title}</th>
            <th>${author}</th>
            <th>${search_reader}</th>
            <th>${search_operation}</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach var="operation" items="${operationlist}">
          <tr class="info">
            <td>${operation.reader.firstName} </td>
            <td>${operation.reader.lastName}</td>
            <td>
              <form action="Task6" method="post">
                <input type="hidden" name="command" value="search_reader_by_id_command" />
                <input type="hidden" name="id" value=${operation.reader.id}>
                <input type="submit" class="btn" value="${searchbyidbutton}" />
              </form>
            </td>
            <td>
              <form action="Task6" method="post">
                <input type="hidden" name="command" value="search_operation_by_id_command" />
                <input type="hidden" name="id" value=${operation.id}>
                <input type="submit" class="btn" value="${searchbyidbutton}" />
              </form>
            </td>
          </tr>
          </c:forEach>
          </tbody>
        </table>
      <form class="form-inline" action="Task6" method="post">
        <input type="hidden" name="command" value="go_to_command" />
        <input type="hidden" name="page" value="/WEB-INF/jsp/editBook.jsp"/>
        <button type="submit" class="btn">${edit_book_button}</button>
      </form>

      <form class="form-inline" action="Task6" method="post">
        <input type="hidden" name="command" value="delete_book_command" />
        <button type="submit" class="btn">${delete_book_button}</button>
      </form>
      <form class="form-inline" action="Task6" method="post">
        <input type="hidden" name="command" value="give_book_command" />
        <label class="checkbox">
          <input type="checkbox" name="reading_room" value="true"> ${readingroom}
        </label>
        <button type="submit" class="btn">${give_book}</button>
      </form>
        <h3><c:out value="${messageloc}"/> </h3>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script>
</form>
</body>
</html>