<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
  <head>
    <title>Search Reader</title>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="resources.localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.librarian.searchbyname" var="searchbynamebutton"/>
    <fmt:message bundle="${loc}" key="local.librarian.searchbylastname" var="searchbylastnamebutton"/>
    <fmt:message bundle="${loc}" key="local.librarian.searchbyid" var="searchbyidbutton"/>
    <fmt:message bundle="${loc}" key="local.librarian.searchall" var="searchallbutton"/>
    <fmt:message bundle="${loc}" key="local.librarian.firstname" var="firstnameinput"/>
    <fmt:message bundle="${loc}" key="local.librarian.lastname" var="lastnameinput"/>
    <fmt:message bundle="${loc}" key="local.gobutton.name" var="gobutton"/>
    <fmt:message bundle="${loc}" key="local.librarian.id" var="idinput"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
    <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
    <fmt:message bundle="${loc}" key="local.search.reader" var="search_reader"/>
    <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
    <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
    <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body >
  <div class="container">
    <header><h1>Библиотека "Хата-читальня"</h1> </header>
    <div class="navbar navbar-static-top navbar-inverse">
      <div class="navbar-inner">
        <a class="navbar-form brand pull-left" href="#">Menu</a>
        <ul class="nav">
          <li class="divider-vertical">
          </li>
          <li class="navbar-form pull-left">
            <form action="Task6" method="post"/>
            <input type="hidden" name="command" value="go_to_command" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
            <input type="submit" class="btn"  value="${login_page}" /><br />
            </form>
          </li>
          <li class="divider-vertical">
          </li>
          <li class="navbar-form pull-left">
            <form action="Task6" method="post"/>
            <input type="hidden" name="command" value="go_to_menu_command" />
            <input type="submit" class="btn"  value="${home}" /><br />
            </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li  class="navbar-form pull-right">
            <form action="Task6" method="post" >
              <input type="hidden" name="local" value="ru" />
              <input type="hidden" name="page" value="/WEB-INF/jsp/searchReader.jsp"/>
              <input type="hidden" name="command" value="change_locale_command" />
              <input type="submit" class="btn" value="${ru_button}" />
            </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li class="navbar-form pull-right">
            <form action="Task6" method="post" >
              <input type="hidden" name="local" value="en"/>
              <input type="hidden" name="page" value="/WEB-INF/jsp/searchReader.jsp"/>
              <input type="hidden" name="command" value="change_locale_command" />
              <input type="submit" class="btn" value="${en_button}" /><br />
            </form>
          </li>
          <li class="divider-vertical" > </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="span11  offset1">
        <legend>${search_reader} </legend>
  <form id="search"  action="Task6" method="post" class="form-horizontal" >
    <input type="submit" value="${gobutton}" form="search"/>
    <label class="radio" >
      <input type="radio" name="command" value="search_reader_by_lastname_command" checked form="search">
      <h4>${searchbylastnamebutton}</h4>
    </label>
    <label class="radio">
      <input type="radio" name="command" value="search_reader_by_name_command" form="search">
      <h4> ${searchbynamebutton}</h4>
    </label>
    <label class="radio">
      <input type="radio" name="command" value="search_reader_by_id_command"  form="search">
      <h4>${searchbyidbutton}</h4>
    </label>
    <label class="radio">
      <input type="radio" name="command" value="search_reader_all_command" checked form="search">
      <h4> ${searchallbutton}</h4>
    </label>
    <div class="control-group">
      <label class="control-label"> <h4>${idinput}</h4></label>
      <div class="controls">
        <input type="text" name="id" pattern="[0-9]+" placeholder="${idinput}">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"> <h4>${firstnameinput}</h4></label>
      <div class="controls">
        <input type="text" name="firstname" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder="${firstnameinput}">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"> <h4>${lastnameinput}</h4></label>
      <div class="controls">
        <input type="text" name="lastname" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder="${lastnameinput}">
      </div>
    </div>
    </form>
        <h3><c:out value="${messageloc}"/> </h3>
      </div>
    </div>
  </div>
  <script src="js/bootstrap.min.js"></script>
  </body>
</html>
