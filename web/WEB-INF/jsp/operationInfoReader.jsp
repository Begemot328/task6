<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
  <title>Operation info reader page</title>
  <fmt:setLocale value="${sessionScope.local}"/>
  <fmt:setBundle basename="resources.localization.local" var="loc"/>
  <fmt:message bundle="${loc}" key="local.reader.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
  <fmt:message bundle="${loc}" key="local.menu.editbook" var="edit_book_button"/>
  <fmt:message bundle="${loc}" key="local.menu.deletebook" var="delete_book_button"/>
  <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
  <fmt:message bundle="${loc}" key="local.operation.info" var="operation_info"/>
  <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
  <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
  <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
  <fmt:message bundle="${loc}" key="local.book.recieve" var="recieve_book"/>
  <fmt:message bundle="${loc}" key="local.menu.operationlist" var="operation_list"/>
  <fmt:message bundle="${loc}" key="local.menu.order" var="order"/>
  <fmt:message bundle="${loc}" key="local.menu.readingroom" var="readingroom"/>
  <fmt:message bundle="${loc}" key="local.operation.deadline.date" var="deadlinedate"/>
  <fmt:message bundle="${loc}" key="local.operation.returning.date" var="returningdate"/>
  <fmt:message bundle="${loc}" key="local.operation.taking.date" var="takingdate"/>
  <fmt:message bundle="${loc}" key="local.book.title" var="title"/>
  <fmt:message bundle="${loc}" key="local.book.author" var="author"/>
  <fmt:message bundle="${loc}" key="local.librarian.firstname" var="firstname"/>
  <fmt:message bundle="${loc}" key="local.librarian.lastname" var="lastname"/>
  <fmt:message bundle="${loc}" key="local.yes" var="yes"/>
  <fmt:message bundle="${loc}" key="local.no" var="no"/>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body >
<div class="container">
  <header><h1>Библиотека "Хата-читальня"</h1> </header>
  <div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
      <a class="navbar-form brand pull-left" href="#">Menu</a>
      <ul class="nav">
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
          <input type="submit" class="btn"  value="${login_page}" /><br />
          </form>
        </li>
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_menu_command" />
          <input type="submit" class="btn"  value="${home}" /><br />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/operationInfoReader.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/operationInfoReader.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
          </form>
        </li>
        <li class="divider-vertical" > </li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="span11  offset1">
      <legend>${operation_info} </legend>
      <table class="table">
        <caption>${operation_list} : ${operation.book.title} ${operation.book.author}</caption>
        <thead>
        <tr class="success">
          <th>${firstname}</th>
          <th>${lastname}</th>
          <th>${title}</th>
          <th>${author}</th>
          <th>${takingdate}</th>
          <th>${returningdate}</th>
          <th>${deadlinedate}</th>
          <th>${readingroom}</th>
          <th>${order}</th>
        </tr>
        </thead>
        <tbody>
        <tr class="info">
          <td>${operation.reader.firstName}</td>
          <td>${operation.reader.lastName}</td>
          <td>${operation.book.title}</td>
          <td>${operation.book.author}</td>
          <td>${operation.takingDate}</td>
          <td>${operation.returningDate}</td>
          <td>${operation.deadlineDate}</td>
          <td>
            <c:choose>
              <c:when test="${operation.readingRoom}"> ${yes} </c:when>
              <c:otherwise> ${no} </c:otherwise>
            </c:choose>
          </td>
          <td>
            <c:choose>
            <c:when test="${operation.order}"> ${yes} </c:when>
            <c:otherwise> ${no} </c:otherwise>
            </c:choose>
          </td>
        </tr>
        </tbody>
      </table>

      <h3><c:out value="${messageloc}"/> </h3>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>