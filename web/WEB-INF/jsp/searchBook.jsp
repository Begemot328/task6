<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
  <head>
    <title>Search Book Page</title>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="resources.localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.book.searchbytitle" var="searchbytitlebutton"/>
    <fmt:message bundle="${loc}" key="local.book.searchbyauthor" var="searchbyauthorbutton"/>
    <fmt:message bundle="${loc}" key="local.book.searchbyid" var="searchbyidbutton"/>
    <fmt:message bundle="${loc}" key="local.book.searchall" var="searchallbutton"/>
    <fmt:message bundle="${loc}" key="local.book.title" var="titleinput"/>
    <fmt:message bundle="${loc}" key="local.book.author" var="authorinput"/>
    <fmt:message bundle="${loc}" key="local.gobutton.name" var="gobutton"/>
    <fmt:message bundle="${loc}" key="local.book.id" var="idinput"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
    <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
    <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
    <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
    <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
    <fmt:message bundle="${loc}" key="local.menu.searchbook" var="search_book_button"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
  <div class="container">
    <header><h1>Библиотека "Хата-читальня"</h1> </header>
    <div class="navbar navbar-static-top navbar-inverse">
      <div class="navbar-inner">
        <a class="brand" href="#">Menu</a>
        <ul class="nav">
          <li class="divider-vertical">
          </li>
          <li class="navbar-form pull-left">
            <form action="Task6" method="post"/>
            <input type="hidden" name="command" value="go_to_command" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
            <input type="submit" class="btn"  value="${login_page}" /><br />
            </form>
          </li>
          <li class="divider-vertical">
          </li>
          <li class="navbar-form pull-left">
            <form action="Task6" method="post"/>
            <input type="hidden" name="command" value="go_to_menu_command" />
            <input type="submit" class="btn"  value="${home}" /><br />
            </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li  class="navbar-form pull-right">
            <form action="Task6" method="post" >
              <input type="hidden" name="local" value="ru" />
              <input type="hidden" name="page" value="/WEB-INF/jsp/searchBook.jsp"/>
              <input type="hidden" name="command" value="change_locale_command" />
              <input type="submit" class="btn" value="${ru_button}" />
            </form>
          </li>
          <li class="divider-vertical" >
          </li>
          <li class="navbar-form pull-right">
            <form action="Task6" method="post" >
              <input type="hidden" name="local" value="en"/>
              <input type="hidden" name="page" value="/WEB-INF/jsp/searchBook.jsp"/>
              <input type="hidden" name="command" value="change_locale_command" />
              <input type="submit" class="btn" value="${en_button}" /><br />
            </form>
          </li>
          <li class="divider-vertical" > </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="span11  offset1">
        <legend>${search_book_button} </legend>
        <form id="search"  action="Task6" method="post" class="form-horizontal" >
          <input type="submit" value="${gobutton}" form="search"/>
          <label class="radio" >
            <input type="radio" name="command" value="search_book_by_author_command" form="search">
            <h4>${searchbyauthorbutton}</h4>
          </label>
          <label class="radio">
            <input type="radio" name="command" value="search_book_by_title_command" form="search">
            <h4> ${searchbytitlebutton}</h4>
          </label>
          <label class="radio">
            <input type="radio" name="command" value="search_book_by_id_command" checked form="search">
            <h4>${searchbyidbutton}</h4>
          </label>
          <label class="radio">
            <input type="radio" name="command" value="search_book_all_command" checked form="search">
            <h4> ${searchallbutton}</h4>
          </label>
          <div class="control-group">
            <label class="control-label"> <h4>${idinput}</h4></label>
            <div class="controls">
              <input type="text" name="id" pattern="[0-9]+" placeholder="${idinput}">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label"> <h4>${titleinput}</h4></label>
            <div class="controls">
              <input type="text" name="title" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder="${titleinput}">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label"> <h4>${authorinput}</h4></label>
            <div class="controls">
              <input type="text" name="author" pattern="[a-zA-Z_0-9А-Яа-яЁё]+" placeholder="${authorinput}">
            </div>
          </div>
        </form>
        <h3><c:out value="${messageloc}"/> </h3>
      </div>
    </div>
  </div>
  <script src="js/bootstrap.min.js"></script>
  </body>
</html>
