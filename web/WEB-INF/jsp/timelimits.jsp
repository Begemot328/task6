<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
  <title>Reader info page</title>
  <fmt:setLocale value="${sessionScope.local}"/>
  <fmt:setBundle basename="resources.localization.local" var="loc"/>
  <fmt:message bundle="${loc}" key="local.reader.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.book.searchbyid" var="searchbyidbutton"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
  <fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>
  <fmt:message bundle="${loc}" key="local.timelimit.order.reading" var="timelimits_order_reading"/>
  <fmt:message bundle="${loc}" key="local.timelimit.order" var="timelimits_order"/>
  <fmt:message bundle="${loc}" key="local.timelimit.reading" var="timelimits_reading"/>
  <fmt:message bundle="${loc}" key="local.timelimit" var="timelimits"/>
  <fmt:message bundle="${loc}" key="${message}" var="messageloc"/>
  <fmt:message bundle="${loc}" key="local.search.reader" var="search_reader"/>
  <fmt:message bundle="${loc}" key="local.homebutton" var="home"/>
  <fmt:message bundle="${loc}" key="local.librarian.menu" var="librarian_menu"/>
  <fmt:message bundle="${loc}" key="local.login.page" var="login_page"/>
  <fmt:message bundle="${loc}" key="local.timelimit.update" var="update_timelimit"/>
  <fmt:message bundle="${loc}" key="local.menu.order" var="order"/>
  <fmt:message bundle="${loc}" key="local.menu.readingroom" var="readingroom"/>
  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body >
<div class="container">
  <header><h1>Библиотека "Хата-читальня"</h1> </header>
  <div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
      <a class="navbar-form brand pull-left" href="#">Menu</a>
      <ul class="nav">
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_command" />
          <input type="hidden" name="page" value="/WEB-INF/jsp/index.jsp"/>
          <input type="submit" class="btn"  value="${login_page}" /><br />
          </form>
        </li>
        <li class="divider-vertical">
        </li>
        <li class="navbar-form pull-left">
          <form action="Task6" method="post"/>
          <input type="hidden" name="command" value="go_to_menu_command" />
          <input type="submit" class="btn"  value="${home}" /><br />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li  class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="ru" />
            <input type="hidden" name="page" value="/WEB-INF/jsp/timelimits.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${ru_button}" />
          </form>
        </li>
        <li class="divider-vertical" >
        </li>
        <li class="navbar-form pull-right">
          <form action="Task6" method="post" >
            <input type="hidden" name="local" value="en"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/timelimits.jsp"/>
            <input type="hidden" name="command" value="change_locale_command" />
            <input type="submit" class="btn" value="${en_button}" /><br />
          </form>
        </li>
        <li class="divider-vertical" > </li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="span11  offset1">
      <legend>${timelimits} </legend>
<h4> ${timelimits_reading} ${timelimitlist.get(3)} </h4>
<h4> ${timelimits_order} ${timelimitlist.get(2)} </h4>
<h4> ${timelimits_order_reading} ${timelimitlist.get(1)} </h4>
<h4> ${timelimits} ${timelimitlist.get(0)} </h4>
      <form class="form-inline" action="Task6" method="post">
        <input type="hidden" name="command" value="set_timelimit_command" />
        <input type="text" pattern="[0-9]+" name="timelimit" value="${limit}">
        <label class="checkbox">
        <input type="checkbox" name="reading_room" value="true"> ${readingroom}
      </label>
        <label class="checkbox">
          <input type="checkbox" name="order" value="true"> ${order}
        </label>
        <button type="submit" class="btn">${update_timelimit}</button>
      </form>
      <h3><c:out value="${messageloc}"/> </h3>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>